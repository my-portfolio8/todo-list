import React, { useState } from 'react';
import './index.css';
import ToDoList from './ToDoList';
const App = () =>{
    const[inputList,setInputList] = useState("");
    const[Items,setItems] = useState([]);
    const[search,setSearch] = useState([]);
    const itemEvent = (event) =>{   
        setInputList(event.target.value);
    }
    console.log("item",Items)
    const listOfItems = () =>{
         setItems((oldItems)=>{
            return[...oldItems,inputList];
         });
        setInputList("");
    }
    const deleteItem = (id) =>{
        setItems((oldItems)=>{
            return oldItems.filter((arrElem,index)=>{
                  return index !== id      
                  
            })
        })
    }
   
    const searchEvent = (event) =>{
        Items.map((data)=> setSearch(data === event))
        setItems(search);
    }

    return( 
        <>
            <div className="main_div">
                <div className="center_div">
                
                    <br/>
                    <h1>ToDo List</h1>
                    <br/>
                    <input type="text" placeholder="Add a Items" onChange={(e)=>{itemEvent(e);searchEvent(e.target.value)}}  value={inputList}/>
                    <button onClick={listOfItems}>+</button>
                    <ol>
                        {Items.map((itemval,index)=>{
                            return <ToDoList text = {itemval} key={index} id={index} onSelect={deleteItem}/>
                        })}
                    </ol>
                </div>
            </div>
        </>
    );
}
export default App;
